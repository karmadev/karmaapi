package es.karmadev.api.database.model.sequel;

import es.karmadev.api.object.ObjectUtils;
import lombok.Getter;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Represents an SQL model. This class
 * is intended to be extended when adding
 * external models, instead of using
 * this own implementation. (Even though
 * it would be completely functional)
 */
@Getter
public class SequelModel {

    public final static SequelModel SQLITE = new SequelModel("sqlite", null, "sequel").register();
    public final static SequelModel SQL = new SequelModel("sequel", new String[]{"sql", "mysql"},
            "sqlite").register();

    private final static Set<SequelModel> models = ConcurrentHashMap.newKeySet();
    protected final String name;
    protected final String[] aliases;
    protected final Set<String> compatibilities = new HashSet<>();

    /**
     * Initialize the sequel model
     * @param name the model name
     * @param compatibilities the model compatibilities
     */
    private SequelModel(final String name, final String[] aliases, final String... compatibilities) {
        ObjectUtils.assertNullOrEmpty(name, "Sequel model name cannot be null");

        this.name = name;
        this.aliases = (aliases == null ? new String[0] : aliases);
        this.compatibilities.add(name.toLowerCase());
        if (compatibilities != null)
            for (String comp : compatibilities)
                this.compatibilities.add(comp.toLowerCase());
    }

    /**
     * Register the model
     */
    protected SequelModel register() {
        models.add(this);
        return this;
    }

    /**
     * Get if the sequel model is compatible
     * with the provided one. This method is
     * intended to be overridden by child
     *
     * @param model the model
     * @return if the models are compatible
     */
    public boolean isCompatible(final SequelModel model) {
        return this.compatibilities.contains(model.name.toLowerCase()) ||
                model.compatibilities.contains(this.name.toLowerCase());
    }

    /**
     * Get the string representation of the
     * sql model
     *
     * @return the sql model as string
     */
    @Override
    public String toString() {
        return String.format("{SQL:%s}", this.name);
    }

    /**
     * Get the sequel model hash code, generated
     * through the model name hash code
     *
     * @return the hash code
     */
    @Override
    public int hashCode() {
        return this.name.toLowerCase().hashCode();
    }

    /**
     * Check if the sequel models is
     * the same as the object provided
     *
     * @param obj the object
     * @return if the model is the same
     */
    @Override
    public boolean equals(final Object obj) {
        if (!(obj instanceof SequelModel)) return false;
        SequelModel model = (SequelModel) obj;

        return model.name.equalsIgnoreCase(this.name);
    }

    /**
     * Creates a sequel model
     *
     * @param name the sequel model name
     * @param aliases the sequel model aliases
     * @return if the model was created or
     * false if it already exists
     */
    public static boolean create(final String name, final String... aliases) {
        if (models.stream().anyMatch((model) -> model.name.equalsIgnoreCase(name) ||
                Arrays.stream(model.aliases).anyMatch((m) -> m.equalsIgnoreCase(name)))) return false;

        SequelModel model = new SequelModel(name, aliases);
        model.register();

        return true;
    }

    /**
     * Tries to create the sequel model, if it
     * already exists, it returns null as a
     * failed operation
     *
     * @param name the model name
     * @param aliases the model aliases
     * @return the sequel model
     */
    public static SequelModel createAndGet(final String name, final String... aliases) {
        if (!create(name)) return null;
        return getOrCreate(name, aliases);
    }

    /**
     * Get a sequel name by its
     * name
     *
     * @param name the name
     * @return the sequel model by name
     */
    @Nullable
    public static SequelModel get(final String name) {
        return models.stream().filter((model) -> model.name.equalsIgnoreCase(name) ||
                        Arrays.stream(model.aliases).anyMatch((m) -> m.equalsIgnoreCase(name)))
                .findFirst().orElse(null);
    }

    /**
     * Get a sequel name by its
     * name, if it does not exist, this
     * method will create it.
     * Unlike {@link #createAndGet(String, String...)} this
     * method does never return null
     *
     * @param name the name
     * @param aliases the aliases
     * @return the sequel model by name
     */
    public static SequelModel getOrCreate(final String name, final String... aliases) {
        SequelModel existing = get(name);
        if (existing == null) {
            existing = new SequelModel(name, aliases);
            existing.register();
        }

        return existing;
    }
}
