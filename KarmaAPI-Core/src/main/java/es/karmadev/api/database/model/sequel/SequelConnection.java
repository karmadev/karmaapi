package es.karmadev.api.database.model.sequel;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import es.karmadev.api.database.DatabaseConnection;
import es.karmadev.api.database.model.sequel.hikari.HikariConfigWrapper;
import es.karmadev.api.database.model.sequel.model.SequelDatabase;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * SQL connection wrapper. Relies on HikariCP
 */
public class SequelConnection implements DatabaseConnection {

    private HikariDataSource source;
    private HikariConfigWrapper wrapper = new HikariConfigWrapper();

    private boolean connecting = false;

    /**
     * Start configuring the connection
     *
     * @return the configuration
     */
    public HikariConfigWrapper configure() {
        return this.wrapper;
    }

    /**
     * Pre-configure the connection
     *
     * @param config the existing Hikari config
     */
    public void preConfigure(final HikariConfig config) {
        this.wrapper = HikariConfigWrapper.fromHikari(config);
    }

    /**
     * Pre-configure the connection
     *
     * @param properties the configuration properties
     */
    public void preConfigure(final Properties properties) {
        this.wrapper = new HikariConfigWrapper(properties);
    }

    /**
     * Pre-configure the connection
     *
     * @param propertyFileName the configuration properties path
     */
    public void preConfigure(final String propertyFileName) {
        this.wrapper = new HikariConfigWrapper(propertyFileName);
    }

    /**
     * Pre-configure the connection
     *
     * @param wrapper the existing config wrapper
     */
    public void preConfigure(final HikariConfigWrapper wrapper) {
        this.wrapper = wrapper;
    }

    /**
     * Connect to the SQL server
     */
    public void connect() {
        if (connecting || source.isRunning()) return;
        connecting = true;

        HikariConfig config = this.wrapper.toHikari();
        this.source = new HikariDataSource(config);

        connecting = false;
    }

    /**
     * Get if the connection is
     * established
     *
     * @return if the connection is currently
     * active
     */
    public boolean isConnected() {
        return connecting || source.isRunning();
    }

    /**
     * Get a sequel database
     *
     * @param name the database name
     * @return the database
     */
    public SequelDatabase getDatabase(final String name) {
        Connection connection = null;
        Statement statement = null;
        try {
            connection = source.getConnection();
            statement = connection.createStatement();

            try (ResultSet rs = statement.executeQuery(String.format("USE `%s`", name))) {
                if (rs.next() && !rs.wasNull()) return new SequelDatabase(name, source);
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        } finally {
            close(connection, statement);
        }

        return null;
    }

    /**
     * Get all the available databases. Please note
     * no information gets cached to allow dynamic data
     * to be dynamically visible.
     *
     * @return the databases
     */
    public List<String> getDatabases() {
        List<String> dbNames = new ArrayList<>();

        Connection connection = null;
        Statement statement = null;
        try {
            connection = source.getConnection();
            statement = connection.createStatement();

            try (ResultSet rs = statement.executeQuery("SHOW DATABASES")) {
                while (rs.next()) {
                    String dbName = rs.getString(1);
                    if (rs.wasNull()) continue;

                    dbNames.add(dbName);
                }
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        } finally {
            close(connection, statement);
        }

        return dbNames;
    }

    /**
     * Close a statement
     *
     * @param statement the statement to close
     */
    public static void close(final Statement statement) {
        close(null, statement);
    }

    /**
     * Close a connection
     *
     * @param connection the connection to close
     */
    public static void close(final Connection connection) {
        close(connection, null);
    }

    /**
     * Close a connection and the statement
     *
     * @param connection the connection to close
     * @param statement the statement to close
     */
    public static void close(final Connection connection, final Statement statement) {
        try {
            if (statement == null || statement.isClosed()) return;
            statement.close();
        } catch (Throwable ignored) {}

        try {
            if (connection == null || connection.isClosed()) return;
            connection.close();
        } catch (Throwable ignored) {}
    }
}
