package es.karmadev.api.database.model.sequel.hikari;

import com.zaxxer.hikari.*;
import com.zaxxer.hikari.metrics.MetricsTrackerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.Driver;
import java.util.Properties;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;

/**
 * HikariCP configuration wrapper. This
 * class contains most of the configuration
 * options available in HikariCP config
*/
@SuppressWarnings("unused")
public class HikariConfigWrapper {

    private final HikariConfig config;
    
    /**
     * Creates a new Hikari config wrapper
     */
    public HikariConfigWrapper() {
        this(new HikariConfig());
    }

    /**
     * Creates a new Hikari config wrapper
     * @param properties the config properties
     */
    public HikariConfigWrapper(final Properties properties) {
        this(new HikariConfig(properties));
    }

    /**
     * Creates a new Hikari config wrapper
     * @param propertyFileName the path to the config properties
     */
    public HikariConfigWrapper(final String propertyFileName) {
        this(new HikariConfig(propertyFileName));
    }

    /**
     * Creates a new Hikari config wrapper
     * from the HikariConfig
     * 
     * @param config the existing configuration
     */
    private HikariConfigWrapper(final HikariConfig config) {
        this.config = config;
    }
    
    /**
     * Set the catalog of the connection
     *
     * @param catalog the catalog
     * @return the config wrapper
     */
    public HikariConfigWrapper setCatalog(final String catalog) {
        this.config.setCatalog(catalog);
        return this;
    }
    
    /**
     * Set the connection timeout
     *
     * @param connectionTimeoutMs the timeout in milliseconds
     * @return the config wrapper
     */
    public HikariConfigWrapper setConnectionTimeout(final long connectionTimeoutMs) {
        this.config.setConnectionTimeout(connectionTimeoutMs);
        return this;
    }

    /**
     * Set the idle connections timeout
     *
     * @param idleTimeoutMs the timeout in milliseconds
     * @return the config wrapper
     */
    public HikariConfigWrapper setIdleTimeout(final long idleTimeoutMs) {
        this.config.setIdleTimeout(idleTimeoutMs);
        return this;
    }

    /**
     * Set the leak detection threshold
     *
     * @param leakDetectionThresholdMs the threshold in milliseconds
     * @return the config wrapper
     */
    public HikariConfigWrapper setLeakDetectionThreshold(final long leakDetectionThresholdMs) {
        this.config.setLeakDetectionThreshold(leakDetectionThresholdMs);
        return this;
    }

    /**
     * Set the connections in the pool max
     * time life
     *
     * @param maxLifetimeMs the connection time life in milliseconds
     * @return the config wrapper
     */
    public HikariConfigWrapper setMaxLifetime(final long maxLifetimeMs) {
        this.config.setMaxLifetime(maxLifetimeMs);
        return this;
    }

    /**
     * Set the max pool size for connections
     *
     * @param maxPoolSize the pool size
     * @return the config wrapper
     */
    public HikariConfigWrapper setMaximumPoolSize(final int maxPoolSize) {
        this.config.setMaximumPoolSize(maxPoolSize);
        return this;
    }

    /**
     * Set the minimum connection idle timeout
     *
     * @param minIdle the min idle timeout
     * @return the config wrapper
     */
    public HikariConfigWrapper setMinimumIdle(final int minIdle) {
        this.config.setMinimumIdle(minIdle);
        return this;
    }

    /**
     * Set the default password to use for DataSource.getConnection(username, password) calls.
     *
     * @param password the password
     * @return the config wrapper
     */
    public HikariConfigWrapper setPassword(final String password) {
        this.config.setPassword(password);
        return this;
    }

    /**
     * Set the default username used for DataSource.getConnection(username, password) calls.
     *
     * @param username the username
     * @return the config wrapper
     */
    public HikariConfigWrapper setUsername(final String username) {
        this.config.setUsername(username);
        return this;
    }

    /**
     * Set the validation timeout
     *
     * @param validationTimeoutMs the timeout in milliseconds
     * @return the config wrapper
     */
    public HikariConfigWrapper setValidationTimeout(final long validationTimeoutMs) {
        this.config.setValidationTimeout(validationTimeoutMs);
        return this;
    }

    /**
     * Set the SQL query to be executed to test the validity of connections. Using
     * the JDBC4 <code>Connection.isValid()</code> method to test connection validity can
     * be more efficient on some databases and is recommended.
     *
     * @param connectionTestQuery a SQL query string
     * @return the config wrapper
     */
    public HikariConfigWrapper setConnectionTestQuery(final String connectionTestQuery) {
        this.config.setConnectionTestQuery(connectionTestQuery);
        return this;
    }

    /**
     * Set the SQL string that will be executed on all new connections when they are
     * created, before they are added to the pool.  If this query fails, it will be
     * treated as a failed connection attempt.
     *
     * @param connectionInitSql the SQL to execute on new connections
     * @return the config wrapper
     */
    public HikariConfigWrapper setConnectionInitSql(final String connectionInitSql) {
        this.config.setConnectionInitSql(connectionInitSql);
        return this;
    }

    /**
     * Set a {@link DataSource} for the pool to explicitly wrap.  This setter is not
     * available through property file based initialization.
     *
     * @param dataSource a specific {@link DataSource} to be wrapped by the pool
     * @return the config wrapper
     */
    public HikariConfigWrapper setDataSource(final DataSource dataSource) {
        this.config.setDataSource(dataSource);
        return this;
    }

    /**
     * Set the fully qualified class name of the JDBC {@link DataSource} that will be used create Connections.
     *
     * @param className the fully qualified name of the JDBC {@link DataSource} class
     * @return the config wrapper
     */
    public HikariConfigWrapper setDataSourceClassName(final String className) {
        this.config.setDataSourceClassName(className);
        return this;
    }

    /**
     * Add a property (name/value pair) that will be used to configure the {@link DataSource}/{@link Driver}.
     * <p>
     * In the case of a {@link DataSource}, the property names will be translated to Java setters following the Java Bean
     * naming convention.  For example, the property {@code cachePrepStmts} will translate into {@code setCachePrepStmts()}
     * with the {@code value} passed as a parameter.
     * <p>
     * In the case of a {@link Driver}, the property will be added to a {@link Properties} instance that will
     * be passed to the driver during {@link Driver#connect(String, Properties)} calls.
     *
     * @param propertyName the name of the property
     * @param value        the value to be used by the DataSource/Driver
     * @return the config wrapper
     */
    public HikariConfigWrapper addDataSourceProperty(final String propertyName, final Object value) {
        this.config.addDataSourceProperty(propertyName, value);
        return this;
    }

    /**
     * Set the JNDI data source
     * 
     * @param jndiDataSource the data source
     * @return the config wrapper
     */
    public HikariConfigWrapper setDataSourceJNDI(final String jndiDataSource) {
        this.config.setDataSourceJNDI(jndiDataSource);
        return this;
    }

    /**
     * Set the data source properties
     * 
     * @param dsProperties the properties
     * @return the config wrapper
     */
    public HikariConfigWrapper setDataSourceProperties(final Properties dsProperties) {
        this.config.setDataSourceProperties(dsProperties);
        return this;
    }

    /**
     * Set the driver class name
     * 
     * @param driverClassName the driver
     * @return the config wrapper
     */
    public HikariConfigWrapper setDriverClassName(final String driverClassName) {
        this.config.setDriverClassName(driverClassName);
        return this;
    }

    /**
     * Set the Jdbc URL
     * 
     * @param jdbcUrl the java SQL url
     * @return the config wrapper
     */
    public HikariConfigWrapper setJdbcUrl(final String jdbcUrl) {
        this.config.setJdbcUrl(jdbcUrl);
        return this;
    }

    /**
     * Set the default auto-commit behavior of connections in the pool.
     *
     * @param isAutoCommit the desired auto-commit default for connections
     * @return the config wrapper
     */
    public HikariConfigWrapper setAutoCommit(final boolean isAutoCommit) {
        this.config.setAutoCommit(isAutoCommit);
        return this;
    }

    /**
     * Set whether or not pool suspension is allowed.  There is a performance
     * impact when pool suspension is enabled.  Unless you need it (for a
     * redundancy system for example) do not enable it.
     *
     * @param isAllowPoolSuspension the desired pool suspension allowance
     * @return the config wrapper
     */
    public HikariConfigWrapper setAllowPoolSuspension(final boolean isAllowPoolSuspension) {
        this.config.setAllowPoolSuspension(isAllowPoolSuspension);
        return this;
    }

    /**
     * Set the pool initialization failure timeout.  This setting applies to pool
     * initialization when {@link HikariDataSource} is constructed with a {@link HikariConfig},
     * or when {@link HikariDataSource} is constructed using the no-arg constructor
     * and {@link HikariDataSource#getConnection()} is called.
     * <ul>
     *   <li>Any value greater than zero will be treated as a timeout for pool initialization.
     *       The calling thread will be blocked from continuing until a successful connection
     *       to the database, or until the timeout is reached.  If the timeout is reached, then
     *       a {@code PoolInitializationException} will be thrown. </li>
     *   <li>A value of zero will <i>not</i>  prevent the pool from starting in the
     *       case that a connection cannot be obtained. However, upon start the pool will
     *       attempt to obtain a connection and validate that the {@code connectionTestQuery}
     *       and {@code connectionInitSql} are valid.  If those validations fail, an exception
     *       will be thrown.  If a connection cannot be obtained, the validation is skipped
     *       and the the pool will start and continue to try to obtain connections in the
     *       background.  This can mean that callers to {@code DataSource#getConnection()} may
     *       encounter exceptions. </li>
     *   <li>A value less than zero will bypass any connection attempt and validation during
     *       startup, and therefore the pool will start immediately.  The pool will continue to
     *       try to obtain connections in the background. This can mean that callers to
     *       {@code DataSource#getConnection()} may encounter exceptions. </li>
     * </ul>
     * Note that if this timeout value is greater than or equal to zero (0), and therefore an
     * initial connection validation is performed, this timeout does not override the
     * {@code connectionTimeout} or {@code validationTimeout}; they will be honored before this
     * timeout is applied.  The default value is one millisecond.
     *
     * @param initializationFailTimeout the number of milliseconds before the
     *                                  pool initialization fails, or 0 to validate connection setup but continue with
     *                                  pool start, or less than zero to skip all initialization checks and start the
     *                                  pool without delay.
     * @return the config wrapper
     */
    public HikariConfigWrapper setInitializationFailTimeout(final long initializationFailTimeout) {
        this.config.setInitializationFailTimeout(initializationFailTimeout);
        return this;
    }

    /**
     * Configure whether internal pool queries, principally aliveness checks, will be isolated in their own transaction
     * via {@link Connection#rollback()}.  Defaults to {@code false}.
     *
     * @param isolate {@code true} if internal pool queries should be isolated, {@code false} if not
     * @return the config wrapper
     */
    public HikariConfigWrapper setIsolateInternalQueries(final boolean isolate) {
        this.config.setIsolateInternalQueries(isolate);
        return this;
    }

    /**
     * Set the metrics factory
     * 
     * @param metricsTrackerFactory the tracker factory
     * @return the config wrapper
     */
    public HikariConfigWrapper setMetricsTrackerFactory(final MetricsTrackerFactory metricsTrackerFactory) {
        this.config.setMetricsTrackerFactory(metricsTrackerFactory);
        return this;
    }

    /**
     * Set a MetricRegistry instance to use for registration of metrics used by HikariCP.
     *
     * @param metricRegistry the MetricRegistry instance to use
     * @return the config wrapper
     */
    public HikariConfigWrapper setMetricRegistry(final Object metricRegistry) {
        this.config.setMetricRegistry(metricRegistry);
        return this;
    }

    /**
     * Set the HealthCheckRegistry that will be used for registration of health checks by HikariCP.  Currently only
     * Codahale/DropWizard is supported for health checks.  Default is {@code null}.
     *
     * @param healthCheckRegistry the HealthCheckRegistry to be used
     * @return the config wrapper
     */
    public HikariConfigWrapper setHealthCheckRegistry(final Object healthCheckRegistry) {
        this.config.setHealthCheckRegistry(healthCheckRegistry);
        return this;
    }

    /**
     * Set the Health check properties
     * 
     * @param healthCheckProperties the properties
     * @return the config wrapper
     */
    public HikariConfigWrapper setHealthCheckProperties(final Properties healthCheckProperties) {
        this.config.setHealthCheckProperties(healthCheckProperties);
        return this;
    }


    /**
     * Set the Health check properties
     * 
     * @param key the property key
     * @param value the property value
     * @return the config wrapper
     */
    public HikariConfigWrapper addHealthCheckProperty(final String key, final String value) {
        this.config.addHealthCheckProperty(key, value);
        return this;
    }

    /**
     * This property controls the keepalive interval for a connection in the pool. An in-use connection will never be
     * tested by the keepalive thread, only when it is idle will it be tested.
     *
     * @param keepaliveTimeMs the interval in which connections will be tested for aliveness, thus keeping them alive by the act of checking. Value is in milliseconds, default is 0 (disabled).
     * @return the config wrapper
     */
    public HikariConfigWrapper setKeepaliveTime(final long keepaliveTimeMs) {
        this.config.setKeepaliveTime(keepaliveTimeMs);
        return this;
    }

    /**
     * Configures the Connections to be added to the pool as read-only Connections.
     *
     * @param readOnly {@code true} if the Connections in the pool are read-only, {@code false} if not
     * @return the config wrapper
     */
    public HikariConfigWrapper setReadOnly(final boolean readOnly) {
        this.config.setReadOnly(readOnly);
        return this;
    }

    /**
     * Configures whether HikariCP self-registers the {@link HikariConfigMXBean} and {@link HikariPoolMXBean} in JMX.
     *
     * @param register {@code true} if HikariCP should register MXBeans, {@code false} if it should not
     * @return the config wrapper
     */
    public HikariConfigWrapper setRegisterMbeans(final boolean register) {
        this.config.setRegisterMbeans(register);
        return this;
    }

    /**
     * Set the name of the connection pool.  This is primarily used for the MBean
     * to uniquely identify the pool configuration.
     *
     * @param poolName the name of the connection pool to use
     * @return the config wrapper
     */
    public HikariConfigWrapper setPoolName(final String poolName) {
        this.config.setPoolName(poolName);
        return this;
    }
    
    /**
     * Set the ScheduledExecutorService used for housekeeping.
     *
     * @param executor the ScheduledExecutorService
     * @return the config wrapper
     */
    public HikariConfigWrapper setScheduledExecutor(final ScheduledExecutorService executor) {
        this.config.setScheduledExecutor(executor);
        return this;
    }
    
    /**
     * Set the default schema name to be set on connections.
     *
     * @param schema the name of the default schema
     * @return the config wrapper
     */
    public HikariConfigWrapper setSchema(final String schema) {
        this.config.setSchema(schema);
        return this;
    }
    
    /**
     * Set the user supplied SQLExceptionOverride class name.
     *
     * @param exceptionOverrideClassName the user supplied SQLExceptionOverride class name
     * @see SQLExceptionOverride
     * @return the config wrapper
     */
    public HikariConfigWrapper setExceptionOverrideClassName(final String exceptionOverrideClassName) {
        this.config.setExceptionOverrideClassName(exceptionOverrideClassName);
        return this;
    }

    /**
     * Set the default transaction isolation level.  The specified value is the
     * constant name from the <code>Connection</code> class, eg.
     * <code>TRANSACTION_REPEATABLE_READ</code>.
     *
     * @param isolationLevel the name of the isolation level
     * @return the config wrapper
     */
    public HikariConfigWrapper setTransactionIsolation(final String isolationLevel) {
        this.config.setTransactionIsolation(isolationLevel);
        return this;
    }
    
    /**
     * Set the thread factory to be used to create threads.
     *
     * @param threadFactory the thread factory (setting to null causes the default thread factory to be used)
     * @return the config wrapper
     */
    public HikariConfigWrapper setThreadFactory(final ThreadFactory threadFactory) {
        this.config.setThreadFactory(threadFactory);
        return this;
    }

    /**
     * Copies the state of {@code this} into {@code other}.
     *
     * @param other Other {@link HikariConfig} to copy the state to.
     * @return the config wrapper
     */
    public HikariConfigWrapper copyStateTo(final HikariConfig other) {
        this.config.copyStateTo(other);
        return this;
    }

    /**
     * Copies the state of {@code this} into {@code other}.
     * 
     * @param other Other {@link HikariConfigWrapper} to copy state to
     * @return the config wrapper
     */
    public HikariConfigWrapper copyStateTo(final HikariConfigWrapper other) {
        this.config.copyStateTo(other.config);
        return this;
    }

    /**
     * Validate the configuration
     * @return the config wrapper
     */
    public HikariConfigWrapper validate() {
        this.config.validate();
        return this;
    }

    /**
     * Get the Hikari config
     * 
     * @return the hikari config
     */
    public HikariConfig toHikari() {
        return this.config;
    }
    
    /**
     * Create a config wrapper from an existing
     * Hikari config
     * 
     * @param config the existing config
     * @return the config wrapper
     */
    public static HikariConfigWrapper fromHikari(final HikariConfig config) {
        return new HikariConfigWrapper(config);
    }
}
