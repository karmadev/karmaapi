package es.karmadev.api.database.model.sequel.model.object;

import es.karmadev.api.database.model.sequel.model.object.fetch.MultiFetch;
import es.karmadev.api.database.model.sequel.model.object.mod.RowConstraint;
import lombok.Getter;

import java.util.*;

/**
 * Represents a SQL table
 */
public class Table {

    @Getter
    private final String name;
    private final List<Row<?>> rows = new ArrayList<>();

    /**
     * Initialize the table
     *
     * @param name the table name
     */
    public Table(final String name) {
        this.name = name;
    }

    /**
     * Add a row to the table
     *
     * @param row the row name
     */
    public void addRow(final Row<?> row) {
        this.rows.add(row);
    }

    /**
     * Remove a row from the table
     *
     * @param row the row to remove
     */
    public void removeRow(final Row<?> row) {
        this.rows.remove(row);
    }

    /**
     * Get a table row
     *
     * @param rowIndex the row index
     * @return the row
     * @param <T> the row type
     */
    @SuppressWarnings("unchecked")
    public <T> Row<T> getRow(final int rowIndex) {
        if (rowIndex < 0 || rowIndex >= rows.size()) return null;
        return (Row<T>) rows.get(rowIndex);
    }

    /**
     * Get a table row
     *
     * @param rowName the row name
     * @return the row
     * @param <T> the row type
     */
    @SuppressWarnings("unchecked")
    public <T> Row<T> getRow(final String rowName) {
        Optional<Row<?>> rowMatch = this.rows.stream().filter((row) -> row.getName().equals(rowName))
                .findAny();
        return (Row<T>) rowMatch.orElse(null);
    }

    /**
     * Get all the values of a row
     *
     * @param row the row
     * @return the row values
     * @param <T> the element type
     */
    public <T> Collection<T> getAll(final Row<T> row) {
        //TODO: Logic
        return null;
    }

    /**
     * Get all the values of
     * multiple rows
     *
     * @param rows the rows
     * @return the multi fetch value
     */
    public MultiFetch getAll(final Row<?>... rows) {
        //TODO: Logic
        return null;
    }

    /**
     * Get all the table rows
     *
     * @return the table rows
     */
    public List<Row<?>> getRows() {
        return Collections.unmodifiableList(rows);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("CREATE TABLE IF NOT EXISTS `")
                .append(this.name).append("`(");

        boolean first = true;
        List<RowConstraint> constraints = new ArrayList<>();
        for (Row<?> row : rows) {
            if (first) {
                first = false;
            } else {
                builder.append(", ");
            }

            builder.append(row);
            constraints.addAll(row.getConstraints());
        }

        for (RowConstraint constraint : constraints) {
            builder.append(", ");
            builder.append(constraint);
        }

        return builder.append(")").toString();
    }

    public String toString(final boolean pretty) {
        if (!pretty) return toString();

        StringBuilder builder = new StringBuilder("CREATE TABLE IF NOT EXISTS `")
                .append(this.name).append("`(");

        boolean first = true;
        List<RowConstraint> constraints = new ArrayList<>();
        for (Row<?> row : rows) {
            if (first) {
                first = false;
                builder.append("\n");
            } else {
                builder.append(",\n ");
            }

            builder.append("\t").append(row);
            constraints.addAll(row.getConstraints());
        }

        for (RowConstraint constraint : constraints) {
            builder.append(",\n ");
            builder.append("\t").append(constraint);
        }

        return builder.append("\n)").toString();
    }
}
