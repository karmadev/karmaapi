package es.karmadev.api.database.model.sequel.model.object.mod;

import lombok.Getter;

/**
 * Represents a row constraint
 */
@Getter
public class RowConstraint extends RowModifier {

    /**
     * Create a new composed primary key
     *
     * @param rowName the primary key row name
     * @param others the other primary key row names
     * @return the row constraint
     */
    public static RowConstraint PRIMARY_KEY(final String rowName, final String... others) {
        StringBuilder builder = new StringBuilder("PRIMARY KEY(`");
        builder.append(rowName).append("`");

        for (String other : others) builder.append(" `").append(other).append("`");
        builder.append(")");

        return new RowConstraint("", builder.toString());
    }

    /**
     * Create a new foreign key
     *
     * @param name the foreign key name
     * @return the foreign key
     */
    public static ForeignKey FOREIGN(final String name) {
        return ForeignKey.newForeignKey(name);
    }

    /**
     * Create a new composed unique key
     *
     * @param constName the constraint name
     * @param rowName the unique key row
     * @param others the other unique key row names
     * @return the row constraint
     */
    public static RowConstraint UNIQUE_KEY(final String constName, final String rowName, final String... others) {
        StringBuilder builder = new StringBuilder("UNIQUE(`");
        builder.append(rowName).append("`");

        for (String other : others) builder.append(" `").append(other).append("`");
        builder.append(")");

        return new RowConstraint(constName, builder.toString());
    }

    protected final String value;

    /**
     * Initialize the constraint
     *
     * @param name the constraint name
     * @param value the constraint value
     */
    public RowConstraint(final String name, final String value) {
        super(name);
        this.value = value;
    }

    /**
     * Initialize the constraint
     *
     * @param value the constraint value
     */
    public RowConstraint(final String value) {
        super("");
        this.value = value;
    }

    /**
     * Get the constraint string representation
     *
     * @return the constraint string
     */
    @Override
    public String toString() {
        if (this.name.isEmpty()) return String.format("ADD CONSTRAINT %s", this.value);
        return String.format("CONSTRAINT `%s` %s", this.name, this.value);
    }
}
