package es.karmadev.api.database.model.sequel.query;

import es.karmadev.api.database.model.sequel.SequelModel;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Map;

/**
 * Represents a sequel query
 */
@Getter @AllArgsConstructor(access = AccessLevel.PRIVATE)
public final class Query {

    private final SequelModel model;
    private final String raw;

    private final int tableCreations;
    private final int dbCreations;

    private final int tableAlters;
    private final int dbAlters;

    private final int tableInserts;
    private final int rowUpdates;

    /**
     * Find a part of the query and replace
     * it with the specified replacement
     *
     * @param match the string to look for
     * @param replacement the replacement
     * @return the modified
     */
    public Query bind(final String match, final String replacement) {
        String newQuery = updateQuery(match, replacement);

        return new Query(model, newQuery, tableCreations, dbCreations,
                tableAlters, dbAlters, tableInserts, rowUpdates);
    }

    /**
     * Map the query directly without using a {@link PreparedQuery}.
     * Please note this method expects the parameters
     * to be already sanitized
     *
     * @param parameters the parameters to bind
     * @return the parametrized query
     */
    public Query map(final Map<String, String> parameters) {
        StringBuilder builder = new StringBuilder(this.raw.toLowerCase());
        for (String key : parameters.keySet()) {
            if (key == null) continue;
            String value = parameters.get(key);

            updateQuery(key, value, builder);
        }

        String newQuery = builder.toString();
        return new Query(model, newQuery, tableCreations, dbCreations,
                tableAlters, dbAlters, 0, 0);
    }

    /**
     * Update a query
     *
     * @param match the query match
     * @param replacement the replacement
     * @return the updated query
     */
    private String updateQuery(final String match, final String replacement) {
        StringBuilder builder = new StringBuilder(this.raw.toLowerCase());
        updateQuery(match, replacement, builder);

        return builder.toString();
    }

    /**
     * Update a query
     *
     * @param match the query match
     * @param replacement the replacement
     * @param container the query container
     */
    private void updateQuery(final String match, final String replacement, final StringBuilder container) {
        if (match.isEmpty()) return;

        String lowercaseMatch = match.toLowerCase();

        int lastIndex = 0;
        int index = container.indexOf(lowercaseMatch);

        while (index != -1) {
            container.replace(lastIndex, index, replacement);
            lastIndex = index + match.length();
            index = container.indexOf(lowercaseMatch, lastIndex);
        }
    }
}
