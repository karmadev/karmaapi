package es.karmadev.api.database.model.sequel.model.object.fetch;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

/**
 * Represents a multiple row fetch
 * value
 */
public class MultiFetch {

    private final Collection<FetchResult<?>> results = new ArrayList<>();

    /**
     * Initialize the multi fetch
     *
     * @param results the fetch results
     */
    public MultiFetch(final Collection<FetchResult<?>> results) {
        this.results.addAll(results);
    }

    /**
     * Paginate the multi fetch
     *
     * @param itemsPerPage the items per
     *                     page to read
     * @return the paginated fetch
     */
    public MultiFetch[] paginate(final int itemsPerPage) {
        if (this.results.isEmpty()) return new MultiFetch[0];
        if (itemsPerPage > this.results.size()) return new MultiFetch[]{this};

        int size = this.results.size();
        int pageAmount = size / itemsPerPage;

        MultiFetch[] paginated = new MultiFetch[pageAmount];
        for (int i = 0, currentSkip = 0; i < paginated.length; i++, currentSkip += itemsPerPage) {
            paginated[i] = new MultiFetch(
                    getResults(currentSkip, itemsPerPage)
            );
        }

        return paginated;
    }

    /**
     * Get all the results in a "paginated"
     * limit
     *
     * @param from the index to start to read
     *             from
     * @param amount the count of elements to take
     * @return the results
     */
    public Collection<FetchResult<?>> getResults(final int from, final int amount) {
        if (from > this.results.size() || (from + amount) >= this.results.size()) {
            return this.results.stream().skip(this.results.size() - amount)
                    .collect(Collectors.toList());
        }
        if (from < 0) {
            return this.results.stream().limit(amount)
                    .collect(Collectors.toList());
        }

        return this.results.stream()
                .skip(Math.abs(from)).limit(Math.abs(amount))
                .collect(Collectors.toList());
    }

    /**
     * Get all the results
     *
     * @return the results
     */
    public Collection<FetchResult<?>> getResults() {
        return Collections.unmodifiableCollection(results);
    }
}