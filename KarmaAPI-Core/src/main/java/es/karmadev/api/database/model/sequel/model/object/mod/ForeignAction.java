package es.karmadev.api.database.model.sequel.model.object.mod;

/**
 * Foreign key actions
 */
public enum ForeignAction {
    /**
     * Do nothing
     */
    DO_NOTHING("NO ACTION"),
    /**
     * Set the value to null
     */
    SET_NULL("SET NULL"),
    /**
     * Update on cascade
     */
    CASCADE("CASCADE"),
    /**
     * Restrict the update
     */
    RESTRICT("RESTRICT");

    private final String name;

    ForeignAction(final String name) {
        this.name = name;
    }

    /**
     * Get the action name
     *
     * @return the name
     */
    public String getName() {
        return this.name;
    }
}
