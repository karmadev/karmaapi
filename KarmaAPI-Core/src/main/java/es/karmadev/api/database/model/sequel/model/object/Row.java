package es.karmadev.api.database.model.sequel.model.object;

import es.karmadev.api.database.model.sequel.model.SequelDataType;
import es.karmadev.api.database.model.sequel.model.object.mod.RowConstraint;
import es.karmadev.api.database.model.sequel.model.object.mod.RowModifier;
import lombok.Getter;

import java.util.*;

/**
 * Represents a SQL row
 */
@Getter
public class Row<SequelType> {

    private final String name;
    private final SequelDataType<SequelType> type;
    private final Set<RowModifier> modifiers = new HashSet<>();
    private final Set<RowConstraint> constraints = new HashSet<>();

    /**
     * Create a new row
     *
     * @param name the row name
     * @param type the row type
     * @param modifiers the row modifiers
     */
    public Row(final String name, final SequelDataType<SequelType> type, final RowModifier... modifiers) {
        this.name = name;
        this.type = type;

        if (modifiers != null) {
            for (RowModifier modifier : modifiers) {
                if (modifier instanceof RowConstraint) {
                    RowConstraint c = (RowConstraint) modifier;
                    this.constraints.add(c);
                } else {
                    this.modifiers.add(modifier);
                }
            }
        }
    }

    /**
     * Get all the row modifiers
     *
     * @return the modifiers
     */
    public Collection<RowModifier> getModifiers() {
        return Collections.unmodifiableSet(this.modifiers);
    }

    /**
     * Get all the row constraints
     *
     * @return the row constraints
     */
    public Collection<RowConstraint> getConstraints() {
        return Collections.unmodifiableSet(constraints);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("`")
                .append(this.name).append("`").append(' ')
                .append(this.type.getName());

        for (RowModifier modifier : this.modifiers) {
            builder.append(' ').append(modifier);
        }

        return builder.toString();
    }
}
