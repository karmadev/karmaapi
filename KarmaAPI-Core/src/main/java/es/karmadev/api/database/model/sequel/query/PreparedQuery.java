package es.karmadev.api.database.model.sequel.query;

import es.karmadev.api.database.model.sequel.SequelConnection;
import es.karmadev.api.database.model.sequel.model.SequelDatabase;
import lombok.AllArgsConstructor;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Represents a prepared
 * query
 */
@AllArgsConstructor
public final class PreparedQuery {

    private final SequelDatabase database;
    private final Query query;

    private final Map<String, String> parameters = new ConcurrentHashMap<>();

    /**
     * Bind a parameter to the
     * query
     *
     * @param paramKey the key
     * @param paramValue the value
     * @return the prepared query
     */
    public PreparedQuery bindParam(final String paramKey, final Object paramValue) {
        if (paramValue instanceof CharSequence) {
            CharSequence sequence = (CharSequence) paramValue;
            String raw = sequence.toString();

            parameters.put(paramKey, String.format("'%s'", raw.replace("'", "\\'")));
            //This should help to sanitize the query
            return this;
        }

        if (paramValue instanceof Character) {
            Character character = (Character) paramValue;
            if (character == '\'') {
                parameters.put(paramKey, "\\'");
                return this;
            }

            parameters.put(paramKey, String.format("'%s'", character));
            return this;
        }

        parameters.put(paramKey, String.valueOf(paramValue));
        return this;
    }

    /**
     * Tries to execute the statement
     *
     * @return if the execution was
     * successful
     */
    public boolean execute() {
        try {
            run();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    /**
     * Run the statement
     *
     * @throws SQLException if the query fails to execute
     */
    public void run() throws SQLException {
        Query source = this.query.map(parameters);
        database.run(source);
    }

    /**
     * Run the query
     *
     * @throws SQLException if the query fails to execute
     */
    public ResultSet executeQuery() throws SQLException {
        Query source = this.query.map(parameters);
        return database.executeQuery(source);
    }
}
