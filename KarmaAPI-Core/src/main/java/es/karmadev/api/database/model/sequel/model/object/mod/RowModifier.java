package es.karmadev.api.database.model.sequel.model.object.mod;

import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * Represents a row modifier
 */
@EqualsAndHashCode @Getter
public class RowModifier {

    public final static RowModifier NOT_NULL = new RowModifier("NOT NULL");
    public final static RowModifier PRIMARY_KEY = new RowModifier("PRIMARY KEY");
    public final static RowModifier UNIQUE_KEY = new RowModifier("UNIQUE KEY");
    public final static RowModifier AUTO_INCREMENT = new RowModifier("AUTO_INCREMENT");
    public final static RowModifier UNSIGNED = new RowModifier("UNSIGNED");
    public final static RowModifier CURRENT_TIMESTAMP = new RowModifier("CURRENT_TIMESTAMP()");

    /**
     * Create a row modifier for a
     * data type size limit, for instance:
     * varchar
     *
     * @param limit the limit size
     * @return the row modifier
     */
    public static RowModifier DATA_LIMIT(final int limit) {
        return new RowModifier(String.format("(%d)", limit));
    }

    /**
     * Create a row modifier for a
     * default value
     *
     * @param defaultValue the default value
     * @return the row modifier
     */
    public static RowModifier DEFAULT(final Object defaultValue) {
        String rawDefault = String.valueOf(defaultValue);
        if (defaultValue instanceof CharSequence) {
            CharSequence sequence = (CharSequence) defaultValue;
            rawDefault = String.format("'%s'", sequence.toString().replace("'", "\\'"));
        }

        return new RowModifier(String.format("DEFAULT %s", rawDefault));
    }

    /**
     * Create a row modifier for a update
     * constraint
     *
     * @param action the on update action
     * @return the row modifier
     */
    public static RowModifier ON_UPDATE(final ForeignAction action) {
        return new RowModifier(String.format("ON UPDATE %s", action.getName()));
    }

    /**
     * Create a row modifier for a delete
     * constraint
     *
     * @param action the on delete action
     * @return the row modifier
     */
    public static RowModifier ON_DELETE(final ForeignAction action) {
        return new RowModifier(String.format("ON DELETE %s", action.getName()));
    }

    protected final String name;

    public RowModifier(final String name) {
        this.name = name;
    }

    /**
     * Returns a string representation of the object. In general, the
     * {@code toString} method returns a string that
     * "textually represents" this object. The result should
     * be a concise but informative representation that is easy for a
     * person to read.
     * It is recommended that all subclasses override this method.
     * <p>
     * The {@code toString} method for class {@code Object}
     * returns a string consisting of the name of the class of which the
     * object is an instance, the at-sign character `{@code @}', and
     * the unsigned hexadecimal representation of the hash code of the
     * object. In other words, this method returns a string equal to the
     * value of:
     * <blockquote>
     * <pre>
     * getClass().getName() + '@' + Integer.toHexString(hashCode())
     * </pre></blockquote>
     *
     * @return a string representation of the object.
     */
    @Override
    public String toString() {
        return name;
    }
}
