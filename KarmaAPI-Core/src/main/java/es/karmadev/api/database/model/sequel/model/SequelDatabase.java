package es.karmadev.api.database.model.sequel.model;

import com.zaxxer.hikari.HikariDataSource;
import es.karmadev.api.database.model.sequel.SequelConnection;
import es.karmadev.api.database.model.sequel.query.PreparedQuery;
import es.karmadev.api.database.model.sequel.query.Query;
import lombok.Getter;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Represents a database. The database contains
 * the object
 */
public class SequelDatabase {

    @Getter
    private final String name;
    private final HikariDataSource source;

    public SequelDatabase(final String name, final HikariDataSource source) {
        this.name = name;
        this.source = source;
    }

    /**
     * Execute a query
     *
     * @param query the query to execute
     * @return a prepared query
     */
    public PreparedQuery prepare(final Query query) {
        return new PreparedQuery(this, query);
    }

    /**
     * Tries to execute the query
     *
     * @param query the query to execute
     * @return if the execution was
     * successful
     */
    public boolean execute(final Query query) {
        try {
            run(query);
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    /**
     * Run the query
     *
     * @param query the query to execute
     * @throws SQLException if the query fails to execute
     */
    public void run(final Query query) throws SQLException {
        if (query.getTableInserts() != 0 || query.getRowUpdates() != 0) {
            //TODO: Print a warning blaming the developer for not using PreparedQuery
        }

        Connection connection = null;
        Statement statement = null;
        try {
            connection = source.getConnection();
            statement = connection.createStatement();

            statement.execute(String.format("USE `%s`", this.name));
            statement.execute(query.getRaw());
        } finally {
            SequelConnection.close(connection ,statement);
        }
    }

    /**
     * Run the query
     *
     * @param query the query to execute
     * @throws SQLException if the query fails to execute
     */
    public ResultSet executeQuery(final Query query) throws SQLException {
        if (query.getTableInserts() != 0 || query.getRowUpdates() != 0) {
            //TODO: Print a warning blaming the developer for not using PreparedQuery
        }

        Connection connection = null;
        Statement statement = null;
        try {
            connection = source.getConnection();
            statement = connection.createStatement();

            statement.execute(String.format("USE `%s`", this.name));
            return statement.executeQuery(query.getRaw());
        } finally {
            SequelConnection.close(connection ,statement);
        }
    }
}
