package es.karmadev.api.database.model.sequel.model;

import es.karmadev.api.array.ArrayUtils;
import es.karmadev.api.object.ObjectUtils;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.time.Instant;
import java.util.Arrays;
import java.util.Date;

/**
 * Represents a SQL data type
 * @param <DType> the raw data type.
 */
@EqualsAndHashCode @SuppressWarnings({"unused", "unchecked"})
public class SequelDataType<DType> {

    private final static Class<?>[] PRIMITIVE_OBJECTS = new Class<?>[]{
            Byte.class, Short.class, Integer.class, Long.class, Float.class, Double.class, Boolean.class, Character.class
    };

    public final static Class<?> NULL_ELEMENT_TYPE = NullObject.class;

    public final static SequelDataType<Object> NULL = SequelDataType.typeOf("NULL", NULL_ELEMENT_TYPE);
    public final static SequelDataType<Number> NUMBER = NULL.extend("NUMBER", Number.class);
    public final static SequelDataType<Byte> BIT = NUMBER.extend("BIT", Byte.TYPE);
    public final static SequelDataType<Byte> BYTE = BIT.as("TINYINT");
    public final static SequelDataType<Boolean> BOOLEAN = NULL.extend("BOOLEAN", Boolean.TYPE);
    public final static SequelDataType<Short> SHORT = NUMBER.extend("SMALLINT", Short.TYPE);
    public final static SequelDataType<Integer> INTEGER = NUMBER.extend("INTEGER", Integer.TYPE);
    public final static SequelDataType<Long> BIGINT = NUMBER.extend("BIGINT", Long.TYPE);
    public final static SequelDataType<Float> FLOAT = NUMBER.extend("FLOAT", Float.TYPE);
    public final static SequelDataType<Double> DOUBLE = NUMBER.extend("DOUBLE", Double.TYPE);
    public final static SequelDataType<Double> DECIMAL = DOUBLE.as("DECIMAL");
    public final static SequelDataType<CharSequence> CHAR = NULL.extend("CHAR", CharSequence.class, String.class);
    public final static SequelDataType<CharSequence> VARCHAR = CHAR.as("VARCHAR");
    public final static SequelDataType<Byte> BINARY = BYTE.as("BINARY");
    public final static SequelDataType<Byte> VARBINARY = BINARY.as("VARBINARY");
    public final static SequelDataType<byte[]> TINYBLOB = NULL.extend("TINYBLOB", byte[].class);
    public final static SequelDataType<CharSequence> TINYTEXT = CHAR.as("TINYTEXT");
    public final static SequelDataType<byte[]> BLOB = TINYBLOB.as("BLOB");
    public final static SequelDataType<CharSequence> TEXT = CHAR.as("TEXT");
    public final static SequelDataType<byte[]> MEDIUMBLOB = TINYBLOB.as("MEDIUMBLOB");
    public final static SequelDataType<CharSequence> MEDIUMTEXT = CHAR.as("MEDIUMTEXT");
    public final static SequelDataType<byte[]> LONGBLOB = TINYBLOB.as("LONGBLOB");
    public final static SequelDataType<CharSequence> LONGTEXT = CHAR.as("LONGTEXT");
    public final static SequelDataType<Date> DATE = NULL.extend("DATE", Date.class);
    public final static SequelDataType<Date> DATETIME = DATE.as("DATETIME");
    public final static SequelDataType<Instant> TIMESTAMP = NULL.extend("TIMESTAMP", Instant.class);
    public final static SequelDataType<Date> TIME = DATE.as("TIME");
    public final static SequelDataType<Date> YEAR = DATE.as("YEAR");

    @Getter
    private final String name;
    private final Class<? extends DType>[] types;

    /**
     * Initialize the sequel data type
     *
     * @param name the data type name, it must match
     *             an SQL data type
     * @param types the types
     */
    @SafeVarargs
    private SequelDataType(final String name, final Class<? extends DType>... types) {
        ObjectUtils.assertNullOrEmpty(name, "Data type name cannot be null");
        ObjectUtils.assertNullOrEmpty(types, "Cannot create sequel data type for null", false);

        this.name = name;
        this.types = types;
    }

    /**
     * Get if the object matches the data
     * type
     *
     * @param type the type
     * @return if the object matches the type
     */
    public boolean matches(final Object type) {
        if (type == null) return ArrayUtils.contains(types, NullObject.class);

        Class<?> elType = type.getClass();
        Class<?> subType = asReversedType(elType);

        if (subType != null) return ArrayUtils.containsAny(types, elType, subType);
        return ArrayUtils.contains(types, elType);
    }

    /**
     * Extend the current data type into a new
     * type
     *
     * @param name the new type name
     * @param extraTypes the new type extra
     *                   data types
     * @return the sequel data type
     */
    @SafeVarargs
    public final <A extends DType> SequelDataType<A> extend(final String name,
                                                            final Class<? extends DType>... extraTypes) {
        Class<? extends DType>[] newTypes = ArrayUtils.combine((newAmount) -> Arrays.copyOf(this.types, newAmount),
                this.types, extraTypes);

        return (SequelDataType<A>) new SequelDataType<>(name, newTypes);
    }

    /**
     * Create a new sequel data type with
     * the specified name
     *
     * @param newName the new name
     * @return the sequel data type
     */
    public final SequelDataType<DType> as(final String newName) {
        return new SequelDataType<>(newName, this.types);
    }

    /**
     * Create a new sequel data type in where
     * the value must be not null
     *
     * @return the not null sequel data type
     */
    public SequelDataType<DType> notNull() {
        if (ArrayUtils.contains(this.types, NullObject.class)) {
            int newLength = 0;
            for (Class<?> el : this.types) {
                if (el == null || el.equals(NullObject.class))
                    continue;

                newLength++;
            }

            Class<? extends DType>[] newArray = Arrays.copyOf(this.types, newLength);

            int vIndex = 0;
            for (Class<? extends DType> el : this.types) {
                if (el == null || el.equals(NullObject.class))
                    continue;

                newArray[vIndex++] = el;
            }

            return new SequelDataType<>(name, newArray);
        }

        return this;
    }

    /**
     * Create a new sequel data type in where
     * the value can be null
     *
     * @return the null sequel data type
     */
    public SequelDataType<DType> nullable() {
        if (ArrayUtils.contains(this.types, NullObject.class)) return this;

        Class<? extends DType>[] newArray = Arrays.copyOf(this.types, this.types.length + 1);
        newArray[newArray.length - 1] = (Class<? extends DType>) NullObject.class;

        return new SequelDataType<>(name, newArray);
    }

    private boolean notPrimitiveObject(final Class<?> clazz) {
        return !ArrayUtils.contains(PRIMITIVE_OBJECTS, clazz);
    }

    private Class<?> asReversedType(final Class<?> clazz) {
        if (!clazz.isPrimitive() && notPrimitiveObject(clazz)) return null;

        if (clazz.equals(Byte.class)) {
            return Byte.TYPE;
        } else if (clazz.equals(byte.class)) {
            return Byte.class;
        } else if (clazz.equals(Short.class)) {
            return Short.TYPE;
        } else if (clazz.equals(short.class)) {
            return Short.class;
        } else if (clazz.equals(Integer.class)) {
            return Integer.TYPE;
        } else if (clazz.equals(int.class)) {
            return Integer.class;
        } else if (clazz.equals(Long.class)) {
            return Long.TYPE;
        } else if (clazz.equals(long.class)) {
            return Long.class;
        } else if (clazz.equals(Float.class)) {
            return Float.TYPE;
        } else if (clazz.equals(float.class)) {
            return Float.class;
        } else if (clazz.equals(Double.class)) {
            return Double.TYPE;
        } else if (clazz.equals(double.class)) {
            return Double.class;
        } else if (clazz.equals(Boolean.class)) {
            return Boolean.TYPE;
        } else if (clazz.equals(boolean.class)) {
            return Boolean.class;
        } else if (clazz.equals(Character.class)) {
            return Character.TYPE;
        } else if (clazz.equals(char.class)) {
            return Character.class;
        }

        return clazz;
    }

    /**
     * Create a new sequel data type
     *
     * @param name the type name
     * @param types the type extra types, for instance, INTEGER is Integer.Type, Integer.class and Number.class
     * @return the sequel data type
     * @param <T> the type
     */
    @SafeVarargs
    public static <T> SequelDataType<T> typeOf(final String name, final Class<? extends T>... types) {
        return new SequelDataType<>(name, types);
    }
}

/**
 * Dummy class representing NULL
 */
class NullObject { }
