package es.karmadev.api.database.model.sequel.model.object.fetch;

import es.karmadev.api.database.model.sequel.model.object.Row;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Represents a multi fetch
 * result
 */
@AllArgsConstructor @Getter
public class FetchResult<RType> {

    private final Row<RType> row;
    private final RType value;
}
