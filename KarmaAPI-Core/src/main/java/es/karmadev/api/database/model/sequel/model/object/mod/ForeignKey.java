package es.karmadev.api.database.model.sequel.model.object.mod;

import es.karmadev.api.object.ObjectUtils;

import java.util.ArrayList;
import java.util.List;

public class ForeignKey {

    private final String name;

    private String rowName;

    private String targetTable;
    private String targetRow;

    private final List<RowModifier> extras = new ArrayList<>();

    private ForeignKey(final String name) {
        this.name = name;
    }

    /**
     * Set the foreign key source row
     *
     * @param rowName the source row
     * @return the foreign key
     */
    public ForeignKey forRow(final String rowName) {
        this.rowName = rowName;
        return this;
    }

    /**
     * Set the foreign key target
     * table
     *
     * @param targetTable the target table
     * @return the foreign key
     */
    public ForeignKey inTable(final String targetTable) {
        this.targetTable = targetTable;
        return this;
    }

    /**
     * Set the foreign key target
     * row
     *
     * @param targetRow the target row
     * @return the foreign key
     */
    public ForeignKey targetingRow(final String targetRow) {
        this.targetRow = targetRow;
        return this;
    }

    /**
     * Add a row modifier to the constraint
     *
     * @param modifier the modifier
     * @return the foreign key
     */
    public ForeignKey and(final RowModifier modifier) {
        this.extras.add(modifier);
        return this;
    }

    /**
     * Make the foreign key a
     * constraint query
     *
     * @return the foreign key as a row
     * constraint
     */
    public RowConstraint makeConstraint() {
        ObjectUtils.assertNullOrEmpty(rowName,
                "Row name cannot be null, set it by using ForeignKey#forRow(String)");
        ObjectUtils.assertNullOrEmpty(targetTable,
                "Table name cannot be null, set it by using ForeignKey#inTable(String)");
        ObjectUtils.assertNullOrEmpty(targetRow,
                "Table row name cannot be null, set it by using ForeignKey#targetingRow(String)");

        StringBuilder builder = new StringBuilder();
        builder.append("FOREIGN KEY (`")
                .append(this.rowName).append("`) REFERENCES `")
                .append(this.targetTable).append("`(`")
                .append(this.targetRow).append("`)");

        for (RowModifier extra : extras) {
            builder.append(" ").append(extra);
        }

        return new RowConstraint(this.name, builder.toString());
    }

    static ForeignKey newForeignKey(final String name) {
        return new ForeignKey(name);
    }
}
