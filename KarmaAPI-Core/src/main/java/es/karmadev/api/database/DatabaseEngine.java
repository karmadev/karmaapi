package es.karmadev.api.database;

import es.karmadev.api.core.KarmaKore;
import es.karmadev.api.core.source.APISource;

/**
 * KarmaAPI database engine
 */
public interface DatabaseEngine {

    /**
     * Get if the engine is protected
     *
     * @return if the engine is protected
     */
    default boolean isProtected() { return false; }

    /**
     * Get the engine name
     *
     * @return the engine name
     */
    String getName();

    /**
     * Grab a connection from the engine
     * connection pool (if any)
     *
     * @param name the connection name
     * @return a database connection
     */
    default DatabaseConnection grabConnection(final String name) {
        APISource source = KarmaKore.INSTANCE();
        if (source == null) throw new IllegalStateException("Cannot grab a json connection without the main kore");

        return grabConnection(source, name);
    }

    /**
     * Grab a connection from the engine
     * connection pool (if any)
     *
     * @param source the connection source
     * @param name the connection name
     * @return a database connection
     */
    DatabaseConnection grabConnection(final APISource source, final String name);
}
