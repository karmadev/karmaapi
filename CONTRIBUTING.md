# Contributing
If you want to contribute to KarmaAPI development, you have many ways of doing it.

## Development
You can help by making a [merge request](https://gitlab.com/karmadev/karmaapi/-/merge_requests) of your KarmaAPI fork, by applying a bug fix, security patch or even a new feature

## Donations
You can support me on [buy me a coffee](https://www.buymeacoffee.com/karmadev)
